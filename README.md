## Simple and production ready Tomcat docker images

Modified from `sgrio/tomcat` to provide support for `Oracle Java SE Development Kit 7`

The following extra components and configurations has been applied to the Tomcat server in these images to make it `production ready`:

* Unecessary files(.exe, .bat, etc.) and default applications(like host manager) has been removed to reduce image size
* `APR` support, version `1.6.2`
* `TC Native` support, version `1.2.16`
* `Openssl`, version `1.0.2l`

### Docker Tags

* Tomcat 7
  * Java 7
    * `jdk_7_tomcat_7`: Tomcat version `7.0.81` with `Oracle Java SE Development Kit 7` and `JCE` patched

### Environment 

Environment variables can be set or overridden. For example

```
ENV CATALINA_OPTS="-Dorg.apache.tomcat.util.http.Parameters.MAX_COUNT=10240 -Djava.awt.headless=true -XX:-OmitStackTraceInFastThrow -Djava.util.Arrays.useLegacyMergeSort=true -Xms256m -Xmx4096m -XX:MaxPermSize=1024m -XX:+CMSClassUnloadingEnabled -XX:+UseConcMarkSweepGC -Dcom.sun.management.jmxremote"
```

### Installation

1. Install [Docker](https://www.docker.com/).

2. Download [automated build](https://hub.docker.com/r/theadrum/tomcat/) from public [Docker Hub](https://hub.docker.com/): `docker pull theadrum/tomcat`

### Usage

    docker run -d -p 8080:8080 -v /path/to/webapp.war:/opt/apache-tomcat/webapps/webapp.war theadrum/tomcat


